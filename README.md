# 欢迎来到洛雪音源聚集地
本项目的资源由 **我就是太阳** 编辑和寻找~~厚颜无耻ing~~

## 关于本库
### 转发时请务必带上本仓库的链接，因为制作不易

## 本地音源

<details>
<summary>点击展开</summary>

1. 野花, 野草, IKUN, 六音

```
https://www.123pan.com/s/OF29-UCPxH.html
```

2. flower

**info:** 该链接需要将下载文件后缀的 **.json** 删除

**warning:** 该音源可能与**野花**音源相同

请前往 [发行](https://gitee.com/xy2401128923/lxmusic-music-source/releases) 下载

3. 音源合集

请前往 [发行](https://gitee.com/xy2401128923/lxmusic-music-source/releases) 下载

4. 至尊源

请前往 [发行](https://gitee.com/xy2401128923/lxmusic-music-source/releases) 下载
</details>

## 在线音源

<details>
<summary>点击展开</summary>

1. ~~作者懒得输名字~~

```
https://raw.niuma666bet.buzz/Huibq/keep-alive/master/render_api.js
```

2. IKUN在线版

```
https://raw.kkgithub.com/piko017/-LX-luoxue_yinyuan/master/ikun%E5%85%AC%E7%9B%8A%E9%9F%B3%E6%BA%90.js
```

3. 小熊猫在线版

```
https://raw.kkgithub.com/piko017/-LX-luoxue_yinyuan/master/%E5%B0%8F%E7%86%8A%E7%8C%ABv1.1.1.js
```

4. 六音在线版

```
https://raw.kkgithub.com/piko017/-LX-luoxue_yinyuan/master/%E5%85%AD%E9%9F%B3%E9%9F%B3%E6%BA%90_v1.1.0.js
```
</details>

啊什么？你说无法导入？~~那关我什么事？~~

## 欢迎大家提issue补充
~~也许作者还知道什么奇怪的音源呢¿~~

### 如果内容涉及侵权等奇怪的东西请立刻联系作者删除